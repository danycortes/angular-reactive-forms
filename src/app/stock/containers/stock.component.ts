import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, FormArray, Validators, AbstractControl } from '@angular/forms';

import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/forkJoin';

import { Product, Item } from '../models/product.interface';
import { StockService } from '../services/stock.service';
import { StockValidators } from './stock.validators';


@Component({
    selector: 'stock',
    templateUrl: './stock.component.html',
    styleUrls: ['./stock.component.scss']
})


export class StockComponent implements OnInit {
    form = this.fb.group({
        store: this.fb.group({
            branch: ['', [ Validators.required, StockValidators.checkBranch ], [ this.validateBranch.bind(this) ] ],
            code: ['', Validators.required ]
        }),
        selector: this.createStock({}),
        stock: this.fb.array([])
    }, { validator: StockValidators.checkStockExists });

    products: Product[];
    productMap: Map<number, Product>;
    total: number;

    constructor(
        private fb: FormBuilder,
        private stockService: StockService
    ) {}

    ngOnInit() {
        const cart = this.stockService.getCartItems();
        const products = this.stockService.getProducts();

        Observable.forkJoin( cart, products )
            .subscribe( ([cart, products]: [Item[], Product[]]) => {
                const myMap = products.map<[number, Product]>( product => [product.id, product] );
                this.productMap = new Map <number, Product>( myMap );
                this.products = products;
                cart.forEach( item => this.addStock(item) );

                this.calculateTotal( this.form.get('stock').value );
                this.form.get('stock').valueChanges.subscribe( value => {
                    this.calculateTotal( value );
                });
            } );
    }

    validateBranch( control: AbstractControl ) {
        return this.stockService.checkBranchId( control.value )
                .map( (response: boolean) => {
                    return response ? null : { unknownBranch: true };
                });
    }

    calculateTotal( value: Item[] ) {
        const total = value.reduce( (prev, next) => {
            return prev + ( next.quantity * this.productMap.get( next.product_id ).price );
        }, 0);
        this.total = total;
    }

    onSubmit() {
        console.log( 'Submit:', this.form.value );
    }

    createStock( stock ) {
        return this.fb.group({
            product_id: parseInt( stock.product_id, 10 ) || '' ,
            quantity: stock.quantity || 10
        });
    }

    addStock( stock ) {
        const control = this.form.get('stock') as FormArray;
        control.push( this.createStock(stock) );
    }

    removeStock( { group, index }: { group: FormGroup, index: number} ) {
        const control = this.form.get('stock') as FormArray;
        control.removeAt( index );
    }
}
