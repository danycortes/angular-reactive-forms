import { Component, Input, forwardRef } from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';
import { EmbeddedTemplateAst } from '@angular/compiler';


const COUNTER_CONTROL_ACCESSOR = {
    provide: NG_VALUE_ACCESSOR,
    useExisting: forwardRef( () => StockCounterComponent ),
    multi: true
};


@Component({
    selector: 'stock-counter',
    providers: [ COUNTER_CONTROL_ACCESSOR ],
    templateUrl: './stock-counter.component.html',
    styleUrls: ['./stock-counter.component.scss']
})


export class StockCounterComponent implements ControlValueAccessor {
    private onTouch: Function;
    private onModelChange: Function;
    value: number;
    focus: boolean;
    @Input() step: number;
    @Input() min: number;
    @Input() max: number;


    writeValue( value ) {
        this.value = value || 0;
    }

    registerOnTouched( fn ) {
        this.onTouch = fn;
    }

    registerOnChange( fn ) {
        this.onModelChange = fn;
    }

    increment() {
        if ( this.value < this.max  ) {
            this.value = this.value + this.step;
            this.onModelChange( this.value );
        }
        this.onTouch();
    }

    decrement() {
        if ( this.value > this.min  ) {
            this.value = this.value - this.step;
            this.onModelChange( this.value );
        }
        this.onTouch();
    }

    constructor() {
        this.step = 10;
        this.min = 10;
        this.max = 1000;
        this.value = 10;
    }

    onKeyDown( event: KeyboardEvent ) {
        const handlers = {
            ArrowDown: () => this.decrement(),
            ArrowUp: () => this.increment()
        };

        if ( handlers[event.code] ) {
            handlers[event.code]();
            event.preventDefault();
            event.stopPropagation();
        }
        this.onTouch();
    }

    onBlur( event: FocusEvent ) {
        this.focus = false;
        event.preventDefault();
        event.stopPropagation();
        this.onTouch();
    }

    onFocus( event: FocusEvent ) {
        this.focus = true;
        event.preventDefault();
        event.stopPropagation();
        this.onTouch();
    }
}
