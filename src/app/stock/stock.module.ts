import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

import { StockComponent } from './containers/stock.component';
import { StockBranchComponent } from './components/stock-branch/stock-branch.component';
import { StockSelectorComponent } from './components/stock-selector/stock-selector.component';
import { StockProductsComponent } from './components/stock-products/stock-products.component';
import { StockService } from './services/stock.service';
import { StockCounterComponent } from './components/stock-counter/stock-counter.component';


@NgModule({
    declarations: [
        StockComponent,
        StockBranchComponent,
        StockSelectorComponent,
        StockProductsComponent,
        StockCounterComponent
    ],
    providers: [
        StockService
    ],
    imports: [
        CommonModule,
        ReactiveFormsModule,
        HttpModule
    ],
    exports: [
        StockComponent
    ]
})


export class StockModule {}
